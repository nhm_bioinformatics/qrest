Authentication
==============

*********************
Authentication Module
*********************

.. automodule:: rest_client.auth

Authentication classes 
----------------------

.. autoclass:: RESTAuthentication
	:members:
	:private-members:
	:special-members: __init__

.. autoclass:: UserPassAuth
	:members:
	:private-members:
	:special-members: __init__

.. autoclass:: NetRCAuth
	:members:
	:private-members:
	:special-members: __init__

.. autoclass:: UserPassOrNetRCAuth
	:members:
	:private-members:
	:special-members: __init__

Configuration
-------------

.. autoclass:: UserPassAuthConfig
	:members:
	:private-members:
	:special-members: __init__

.. autoclass:: NetrcOrUserPassAuthConfig
	:members:
	:private-members:
	:special-members: __init__



